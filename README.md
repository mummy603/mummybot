# mummybot

[![pipeline status](https://gitlab.com/mummy603/mummybot/badges/master/pipeline.svg)](https://gitlab.com/mummy603/mummybot/commits/master)

Just another Discord Bot in C#

## Built With
* [Discord.NET](https://github.com/RogueException/Discord.Net)
* [PostgreSQL](https://www.postgresql.org/)
* [EntityFramework Core](https://docs.microsoft.com/en-us/ef/core/)